package com.bunhann.pdf.ui;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.bunhann.pdf.FileUtils;
import com.bunhann.pdf.R;
import com.bunhann.pdf.permission.PermissionsActivity;
import com.bunhann.pdf.permission.PermissionsChecker;

import org.seuksa.itextkhmer.KhmerLigaturizer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.bunhann.pdf.LogUtils.LOGE;
import static com.bunhann.pdf.permission.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.bunhann.pdf.permission.PermissionsChecker.REQUIRED_PERMISSION;

public class MainActivity extends AppCompatActivity {

    Context mContext;

    PermissionsChecker checker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = getApplicationContext();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        checker = new PermissionsChecker(this);

        //createPdf(FileUtils.getAppPath(mContext) + "123.pdf");

        /**
         *
         */
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
                    PermissionsActivity.startActivityForResult(MainActivity.this, PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
                } else {
                    new GeneratePdfTask(FileUtils.getAppPath(mContext) + "123.pdf", view.getContext()).execute();

                }
            }
        });
    }

    public void createPdf(String dest) {

        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
            /**
             * Creating Document
             */
            Document document = new Document();

            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(dest));

            // Open to write
            document.open();

            KhmerLigaturizer d = new KhmerLigaturizer();

            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("Android School");
            document.addCreator("Bunhann");

            /***
             * Variables for further use....
             */
            BaseColor mColorAccent = new BaseColor(0, 153, 204, 255);
            float mHeadingFontSize = 20.0f;
            float mValueFontSize = 26.0f;

            /**
             * How to USE FONT....
             */
            BaseFont urName = BaseFont.createFont("assets/fonts/brandon_light.otf", "UTF-8", BaseFont.EMBEDDED);
            BaseFont khmerFont = BaseFont.createFont("assets/fonts/kh_battambang.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));

            // Title Order Details...
            // Adding Title....

            String strTitle = d.process("ធូ ប៊ុនហាន់");

            //Font mOrderDetailsTitleFont = new Font(urName, 36.0f, Font.NORMAL, BaseColor.BLACK);

            Font mKhmerDetailsTitleFont = new Font(khmerFont, 22, Font.UNDERLINE, BaseColor.BLACK);

            Chunk mKhmerDetailsTitleChunk = new Chunk(strTitle, mKhmerDetailsTitleFont);
            Paragraph mKhmerDetailsTitleParagraph = new Paragraph(mKhmerDetailsTitleChunk);
            mKhmerDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mKhmerDetailsTitleParagraph);

            // Fields of Order Details...
            // Adding Chunks for Title and value
            Font mOrderIdFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Chunk mOrderIdChunk = new Chunk("Order No:", mOrderIdFont);
            Paragraph mOrderIdParagraph = new Paragraph(mOrderIdChunk);
            document.add(mOrderIdParagraph);

            Font mOrderIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderIdValueChunk = new Chunk("#123123", mOrderIdValueFont);
            Paragraph mOrderIdValueParagraph = new Paragraph(mOrderIdValueChunk);
            document.add(mOrderIdValueParagraph);

            // Adding Line Breakable Space....
            document.add(new Paragraph(""));
            // Adding Horizontal Line...
            document.add(new Chunk(lineSeparator));
            // Adding Line Breakable Space....
            document.add(new Paragraph(""));

            // Fields of Order Details...
            Font mOrderDateFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Chunk mOrderDateChunk = new Chunk("Order Date:", mOrderDateFont);
            Paragraph mOrderDateParagraph = new Paragraph(mOrderDateChunk);
            document.add(mOrderDateParagraph);

            Font mOrderDateValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderDateValueChunk = new Chunk("06/07/2017", mOrderDateValueFont);
            Paragraph mOrderDateValueParagraph = new Paragraph(mOrderDateValueChunk);
            document.add(mOrderDateValueParagraph);

            document.add(new Paragraph(""));
            document.add(new Chunk(lineSeparator));
            document.add(new Paragraph(""));

            // Fields of Order Details...
            Font mOrderAcNameFont = new Font(khmerFont, mHeadingFontSize, Font.NORMAL, mColorAccent);

            String strAccount = d.process("ធូ ប៊ុនហាន់");
            Chunk mOrderAcNameChunk = new Chunk(strAccount, mOrderAcNameFont);
            Paragraph mOrderAcNameParagraph = new Paragraph(mOrderAcNameChunk);
            document.add(mOrderAcNameParagraph);

            Font mOrderAcNameValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderAcNameValueChunk = new Chunk("Pratik Butani", mOrderAcNameValueFont);
            Paragraph mOrderAcNameValueParagraph = new Paragraph(mOrderAcNameValueChunk);
            document.add(mOrderAcNameValueParagraph);

            document.add(new Paragraph(""));
            document.add(new Chunk(lineSeparator));
            document.add(new Paragraph(""));
            document.add(new Paragraph(d.process("សាកល្បង ភាសាខ្មែរ"), new Font(khmerFont, 32)));

            document.close();

            Toast.makeText(mContext, "Created... :)", Toast.LENGTH_SHORT).show();

            FileUtils.openFile(mContext, new File(dest));

        } catch (IOException | DocumentException ie) {
            LOGE("createPdf: Error " + ie.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == PermissionsActivity.PERMISSIONS_GRANTED) {
            Toast.makeText(mContext, "Permission Granted to Save", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Permission not granted, Try again!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class GeneratePdfTask extends AsyncTask<String, Void, Void>{

        private String dest;
        private Context context;

        public GeneratePdfTask(String dest, Context context) {
            this.dest = dest;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (new File(dest).exists()) {
                new File(dest).delete();
            }
            Toast.makeText(context, "Please Wait!!!", Toast.LENGTH_LONG).show();

        }

        @Override
        protected Void doInBackground(String... strings) {

            try {
                /**
                 /**
                 * Creating Document
                 */
                Document document = new Document();

                // Location to save
                PdfWriter.getInstance(document, new FileOutputStream(dest));

                // Open to write
                document.open();

                KhmerLigaturizer d = new KhmerLigaturizer();

                // Document Settings
                document.setPageSize(PageSize.A4);
                document.addCreationDate();
                document.addAuthor("Android School");
                document.addCreator("Bunhann");

                /***
                 * Variables for further use....
                 */
                BaseColor mColorAccent = new BaseColor(0, 153, 204, 255);
                float mHeadingFontSize = 20.0f;
                float mValueFontSize = 26.0f;

                /**
                 * How to USE FONT....
                 */
                BaseFont urName = BaseFont.createFont("assets/fonts/brandon_light.otf", "UTF-8", BaseFont.EMBEDDED);
                BaseFont khmerFont = BaseFont.createFont("assets/fonts/kh_battambang.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                // LINE SEPARATOR
                LineSeparator lineSeparator = new LineSeparator();
                lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));

                // Title Order Details...
                // Adding Title....

                String strTitle = d.process("ខ្ញុំស្រឡាញ់ភាសាខ្មែរ");

                //Font mOrderDetailsTitleFont = new Font(urName, 36.0f, Font.NORMAL, BaseColor.BLACK);

                Font mKhmerDetailsTitleFont = new Font(khmerFont, 22, Font.NORMAL, BaseColor.BLACK);

                Chunk mKhmerDetailsTitleChunk = new Chunk(strTitle, mKhmerDetailsTitleFont);
                Paragraph mKhmerDetailsTitleParagraph = new Paragraph(mKhmerDetailsTitleChunk);
                mKhmerDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
                document.add(mKhmerDetailsTitleParagraph);

                // Fields of Order Details...
                // Adding Chunks for Title and value
                Font mOrderIdFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
                Chunk mOrderIdChunk = new Chunk("Order No:", mOrderIdFont);
                Paragraph mOrderIdParagraph = new Paragraph(mOrderIdChunk);
                document.add(mOrderIdParagraph);

                Font mOrderIdValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
                Chunk mOrderIdValueChunk = new Chunk("#123123", mOrderIdValueFont);
                Paragraph mOrderIdValueParagraph = new Paragraph(mOrderIdValueChunk);
                document.add(mOrderIdValueParagraph);

                // Adding Line Breakable Space....
                document.add(new Paragraph(""));
                // Adding Horizontal Line...
                document.add(new Chunk(lineSeparator));
                // Adding Line Breakable Space....
                document.add(new Paragraph(""));

                // Fields of Order Details...
                String dateTitle = d.process("កាលបរិច្ឆេទ ");

                Font mOrderDateFont = new Font(khmerFont, mHeadingFontSize, Font.NORMAL, mColorAccent);
                Chunk mOrderDateChunk = new Chunk(dateTitle + " " + "(Date):", mOrderDateFont);
                Paragraph mOrderDateParagraph = new Paragraph(mOrderDateChunk);
                document.add(mOrderDateParagraph);

                Font mOrderDateValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
                Chunk mOrderDateValueChunk = new Chunk("06/07/2017", mOrderDateValueFont);
                Paragraph mOrderDateValueParagraph = new Paragraph(mOrderDateValueChunk);
                document.add(mOrderDateValueParagraph);

                document.add(new Paragraph(""));
                document.add(new Chunk(lineSeparator));
                document.add(new Paragraph(""));

                // Fields of Order Details...
                Font mOrderAcNameFont = new Font(khmerFont, mHeadingFontSize, Font.NORMAL, mColorAccent);

                String strAccount = d.process("ភាសាខ្មែរសម្រាប់អ្នកទាំងអស់គ្នា");
                Chunk mOrderAcNameChunk = new Chunk(strAccount, mOrderAcNameFont);
                Paragraph mOrderAcNameParagraph = new Paragraph(mOrderAcNameChunk);
                document.add(mOrderAcNameParagraph);

                Font mOrderAcNameValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
                Chunk mOrderAcNameValueChunk = new Chunk("Khmer Language", mOrderAcNameValueFont);
                Paragraph mOrderAcNameValueParagraph = new Paragraph(mOrderAcNameValueChunk);
                document.add(mOrderAcNameValueParagraph);

                document.add(new Paragraph(""));
                document.add(new Chunk(lineSeparator));
                document.add(new Paragraph(""));
                document.add(new Paragraph(d.process("សាកល្បង ភាសាខ្មែរ"), new Font(khmerFont, 32)));

                document.close();



            } catch (IOException | DocumentException ie) {
                LOGE("createPdf: Error " + ie.getLocalizedMessage());
            }catch (ActivityNotFoundException ae) {
                Toast.makeText(context, "No application found to open this file.", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Toast.makeText(context, "Created... :)", Toast.LENGTH_SHORT).show();
            try {
                FileUtils.openFile(context, new File(dest));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
